# 前端知识学习
## 实现网页HTML
HTML 超文本标记语言
### 和txt纯文本来比较
1）HTML网页可以表示文字、图片、视频、音频、动画
都是txt所无法表达
hello.html它里面只能写纯文字，这里就是通过很多标签来表达，浏览器会解析这些标签
以相应形式来展现。

<div>实现第一个网页</div> 标签有开始有结束，通常标签标识形式
<img src="a.jpg"/> 有几个标签比较特殊，一个，结尾/>

### 展示一个网页，HbuilderX提供两种方式
1) 点击工具栏上面箭头，浏览器chrome访问，以一个服务形式来访问  
HbuilderX启动node服务，端口8848
http://127.0.0.1:8848/web2012222/day01/01-hello.html

2）html静态页面，它可以直接使用浏览器打开
file://协议头，访问本地文件
file:///C:/2012/web2012222/day01/01-hello.html

### 实现第一个网页
<html> 代表一个网页  
	<head> 头信息  
		<title></title>浏览页夹标题  
	</head>  
	<body> 身体信息，网页上展示内容都应该写在这里  
		<h1>我的第一html页面</h1> 标题  
	</body>  
</html>  

网页是一种不严谨语言，java严谨写错了就报错，html脚本语言，
是一种弱语言，尽量去展示


### 永和小票
HTML技术，标签，页面展现<tag>  
CSS style样式表，美化页面  

###小结：
1）gitee仓库  
和本地仓库进行配置：init初始化、指定目录和remote 远程仓库进行挂接  
把本地代码提交到gitee的仓库，团队分享  

初始化和挂接远程仓库，只需要配置一次  

2）每日都要执行多次操作  
git add . 点代表当前工作空间的此目录下所有变化（新增文件，修改文件内容，修改文件名，删除文件）  
git add mst.sql 指定文件，就只写文件名  
git commit -m "submit" 参数-m 字符串为什么提交？原因  
git push -u origin master 指定推送到主分支 master  
git push 默认master  

3）分支 branch  
master 主分支，可以产生很多 fork 分支  

### 作业：
1）margin和padding区别？  
2）谷歌二维码？  
3）电影票  